/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import Dao.VentasDao;
import java.text.ParseException;
import java.util.Date;
import java.time.LocalDate;
import java.time.ZoneId;
import Dto.Usuarios;
import Dto.Ventas;

/**
 *
 * @author Admin
 */
public class test {

    public static void main(String[] args) throws ParseException {
        Usuarios c = new Usuarios();
        c.setIdUsuario(1);
        //Obtenemos la fecha de compra del día
        LocalDate today = LocalDate.now();
        //Parseamos la fecha a formato Date
        Date fecha = Date.from(today.atStartOfDay(ZoneId.systemDefault()).toInstant());
        Ventas v = new Ventas(0, "Pagado", fecha, c);
        VentasDao vdao = new VentasDao();
        int idVenta = vdao.createVenta(v);
        System.out.println(idVenta);
    }
}
